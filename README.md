# Space in Hessen Community Website 
<img src="docs/graphics/space_in_hessen_logo_v1.png"  width="120" height="120">



# Requirements
- content maintanability
  - decentralisation: every business enters their own credentials
  - form on website -> email to reviewer -> reply "Yes" -> updated on website
  - pre-check
- Browse by category/tag
  - space hardware
  - space service
  - ground equipment
  - infrastructure
  - academia
- each business in its own (standardised? form (Steckbrief))
- ? offer way to state interest in topic X ?
- link to job offers


# Tasks for an ADP

# Research
- What could be possible interested groups (on website, behind the screen)
- How to classify the Entities (find the tags)

# Identify Reqs
- what to show for whole industry
- what to show for each entity
- how to decentralise entry

## Implementation
- website with proper tech
  - browse whole industry
  - browse individual businesses
  - from to enter own business

Techstack: docker

---
---

# Entities
- Eumentsat
- Telespazio
- LSE
- Solenix
- Vision Space
- tesat
- cora maps
- Lunar Resources Registry
- OHB Digital
- TU Darmstadt
- ESA BIC / cesah
- ESA/ESOC
- hessische Staatskanzlei
- Weltraumkoordinator
- TUDSaT


# Academics
- TU Darmstadt
- Gießen

# StartUp Incubators
- ESA BIC